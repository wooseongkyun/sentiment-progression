# sentiment-progression

This model forcasts the post-call sentiment of the customer. The model has been trained on customer sentiments only. 


## Install libraries
`pip install tensorflow`

## Save & load the model 
```
tf.saved_model.save(one_step_model, 'one_step')
one_step_reloaded = tf.saved_model.load('one_step')
```

## Make predictions

Input in this example is the sequence "NENPPNE". N stands for Neutral, E for Negative, and P for Positive. 
The output of the code is this original sequence plus the new charchter at the end of the output string. 

```
start = time.time()
states = None
next_char = tf.constant(['NENPPNE'])
result = [next_char]
num_chars = 1 # the number of characters to forcast 

for n in range(num_chars):
  next_char, states = one_step_reloaded.generate_one_step(next_char, states=states)
  result.append(next_char)

result = tf.strings.join(result)
end = time.time()
print(result[0].numpy().decode('utf-8'), '\n\n' + '_'*80)
print('\nRun time:', end - start)
```


